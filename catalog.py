"""
navichan - web application to display information of internet boards
Copyright (C) 2018 lain7

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import json
import time
from imageboards import *
from os.path import expanduser, isfile
from settings import WORKINGDIR
from sys import argv, stderr

class catalog():
  def __init__(self):
    self.threads = []
    self.theme = "none"
    self.boards = []
    self.sortMethod = "lastModified"
    self.bgColor = "#EEF2FF"
    self.fgColor = "black"
    self.filterFile = ""
  # Filters out threads #
  def filter(self, threads, filterFile):
    if (isfile(WORKINGDIR + "filters/" + filterFile)):
      filterFile = "filters/" + filterFile
    elif (isfile(expanduser("~.config/navichan/filters/") + filterFile)):
      filterFile = ".config/navichan/filters/" + filterFile
    for i in range(len(argv)):
      if (argv[i] == "-f" or argv[i] =="--filterdir"):
        filterFile = argv[i+1] + filterFile
    if (not isfile(filterFile)):
      stderr.write(time.asctime() + " - \"" + filterFile + "\" not found in filters/ or ~/.config/navichan/filters/\n")
      return threads
    filteredThreads = []
    filterFile = open(filterFile, "r", encoding="UTF-8")
    try:
      threadFilter = json.loads(filterFile.read())
    except:
      stderr.write(time.asctime() + " - \"" + filterFile + "\" is not a proper JSON file. Not applying filter!\n")
      filterFile.close()
      return
    filterFile.close()
    # checks the title and main text for key words             #
    for i in range(len(threads)):
      # checks title and main for words found in threadFilter #
      if (any(x.upper() in threads[i]["title"].upper() + threads[i]["main"].upper() for x in threadFilter)):
        continue
      filteredThreads.append(threads[i])
    return filteredThreads
  # Sorts the thread from highest METHOD to lowest #
  def sort(self, threads, sortMethod):
    sortedThread = []
    while (threads):
      # [number, index] #
      high = [0, 0]
      for i in range(len(threads)):
        if (int(threads[i][sortMethod]) > high[0]):
          high[0] = int(threads[i][sortMethod])
          high[1] = i
      sortedThread.append(threads[high[1]])
      del threads[high[1]]
    return sortedThread
