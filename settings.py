"""
navichan - web application to display information of internet boards
Copyright (C) 2018 lain7

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

VERSION = "0.0.4"
# If you want to run the server outside of the main directory, this #
# should be set to the path to navichan's main directory.           #
WORKINGDIR = ""
