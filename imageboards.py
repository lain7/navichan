"""
navichan - web application to display information of internet boards
Copyright (C) 2018 lain7

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

# The functions in this file are for getting catalogs from various #
# chans. They all use official JSON APIs provided by the chans     #
# themselves. For user text, please use a regular expression to    #
# clean any potential html / in-line javascript. Things can get    #
# messy / dangerous otherwise.                                     #
import json, urllib.request, re

# 4chan #
def get4Catalog(board):
  # gets json from board #
  request = urllib.request.Request("https://a.4cdn.org/" + board + "/catalog.json" , None, {'User-Agent' : 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7', })
  boardCatalog = json.loads(urllib.request.urlopen(request).read().decode("UTF-8"))
  threads = []
  for i in range(len(boardCatalog)):
    for x in range(len(boardCatalog[i]["threads"])):
      chanThread = { "title" : '', "main" : '', "link" : '', "img" : '', "images" : '', "replies" : '', "lastModified" : '', "creationDate" : ''}
      # Some boards don't allow a subject line for a thread #
      if ("sub" in boardCatalog[i]["threads"][x]):
          # clean up html #
          chanThread["title"] = re.sub(re.compile("<.*?>"), '', boardCatalog[i]['threads'][x]['sub'])
      if ("com" in boardCatalog[i]["threads"][x]):
        # clean up html and cut off text #
        chanThread["main"] = re.sub(re.compile("<.*?>"), '', boardCatalog[i]['threads'][x]['com'])
      chanThread["link"] = "https://boards.4chan.org/" + board + "/thread/" + str(boardCatalog[i]["threads"][x]["no"])
      # it's rare, but some threads do not have an image at all #
      if ("tim" in boardCatalog[i]["threads"][x]):
        chanThread["img"] = "https://i.4cdn.org/" + board + "/" + str(boardCatalog[i]["threads"][x]["tim"]) + "s.jpg"
      else:
        chanThread["img"] = "https://s.4cdn.org/image/error/404/404-Angelguy.png"
      chanThread["replies"] = str(boardCatalog[i]["threads"][x]["replies"])
      chanThread["images"] = str(boardCatalog[i]["threads"][x]["images"])
      chanThread["lastModified"] = str(boardCatalog[i]["threads"][x]["last_modified"])
      chanThread["creationDate"] = str(boardCatalog[i]["threads"][x]["time"])
      threads.append(chanThread)
  return threads

# 8chan #
def get8Catalog(board):
  # gets json from board #
  request = urllib.request.Request("https://8ch.net/" + board + "/catalog.json" , None, {'User-Agent' : 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7', })
  boardCatalog = json.loads(urllib.request.urlopen(request).read().decode("UTF-8"))
  threads = []
  for i in range(len(boardCatalog)):
    for x in range(len(boardCatalog[i]["threads"])):
      chanThread = { "title" : '', "main" : '', "link" : '', "img" : '', "images" : '', "replies" : '', "lastModified" : '', "creationDate" : ''}
      # Some boards don't allow a subject line for a thread #
      if ("sub" in boardCatalog[i]["threads"][x]):
          # clean up html #
          chanThread["title"] = re.sub(re.compile("<.*?>"), '', boardCatalog[i]['threads'][x]['sub'])
      if ("com" in boardCatalog[i]["threads"][x]):
        # clean up html and cut off text #
        chanThread["main"] = re.sub(re.compile("<.*?>"), '', boardCatalog[i]['threads'][x]['com'])
      chanThread["link"] = "https://8ch.net/" + board + "/res/" + str(boardCatalog[i]["threads"][x]["no"]) + ".html"
      # some threads do not have an image at all #
      path = ""
      if ("tim" in boardCatalog[i]["threads"][x]):
        # some older image names are not 64 characters in length #
        if (len(boardCatalog[i]["threads"][x]["tim"]) != 64):
          path = board + "/thumb/"
          if (boardCatalog[i]["threads"][x]["ext"] == ".gif" and "md5" in boardCatalog[i]["threads"][x]):
            boardCatalog[i]["threads"][x].update({"ext" : ".jpg"})
          else:
            # most thumbnails at this point are jpg #
            boardCatalog[i]["threads"][x].update({"ext" : ".jpg"})
        else:
          path = "file_store/thumb/"
        if(not str(boardCatalog[i]["threads"][x]["ext"]) in [".png", ".jpg", ".jpeg", ".gif"]):
          chanThread["img"] = "https://media.8ch.net/static/file.png"
        else:
          chanThread["img"] = "https://media.8ch.net/" + path + str(boardCatalog[i]["threads"][x]["tim"]) + boardCatalog[i]["threads"][x]["ext"]
      else:
        chanThread["img"] = "https://media.8ch.net/static/no-file.png"
      chanThread["replies"] = str(boardCatalog[i]["threads"][x]["replies"])
      chanThread["images"] = str(boardCatalog[i]["threads"][x]["images"] + boardCatalog[i]["threads"][x]["omitted_images"])
      chanThread["lastModified"] = str(boardCatalog[i]["threads"][x]["last_modified"])
      chanThread["creationDate"] = str(boardCatalog[i]["threads"][x]["time"])
      threads.append(chanThread)
  return threads

# arisuchan #
def getArisuCatalog(board):
  # some of arisuchans main boards have unicode characters #
  if(board == "lambda"):
    board = urllib.parse.quote('λ')
  elif(board == "delta"):
    board = urllib.parse.quote('Δ')
  elif(board in ['Δ', 'λ']):
    board = urllib.parse.quote(board)
  # gets json from board #
  request = urllib.request.Request("https://arisuchan.jp/" + board + "/catalog.json" , None, {'User-Agent' : 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7', })
  boardCatalog = json.loads(urllib.request.urlopen(request).read().decode("UTF-8"))
  threads = []
  for i in range(len(boardCatalog)):
    for x in range(len(boardCatalog[i]["threads"])):
      chanThread = { "title" : '', "main" : '', "link" : '', "img" : '', "images" : '', "replies" : '', "lastModified" : '', "creationDate" : ''}
      # Some boards don't allow a subject line for a thread #
      if ("sub" in boardCatalog[i]["threads"][x]):
          # clean html *
          chanThread["title"] = re.sub(re.compile("<.*?>"), '', boardCatalog[i]['threads'][x]['sub'])
      if ("com" in boardCatalog[i]["threads"][x]):
        # clean up html #
        chanThread["main"] = re.sub(re.compile("<.*?>"), '', boardCatalog[i]['threads'][x]['com'])
      chanThread["link"] = "https://arisuchan.jp/" + board + "/res/" + str(boardCatalog[i]["threads"][x]["no"]) + ".html"
      # some threads do not have an image at all #
      if ("tim" in boardCatalog[i]["threads"][x]):
        chanThread["img"] = "https://arisuchan.jp/" + board + "/thumb/" + str(boardCatalog[i]["threads"][x]["tim"]) + ".png"
      if(boardCatalog[i]["threads"][x]["ext"] == ".pdf"):
        chanThread["img"] = "https://arisuchan.jp/static/pdf.png"
      chanThread["replies"] = str(boardCatalog[i]["threads"][x]["replies"])
      chanThread["images"] = str(boardCatalog[i]["threads"][x]["images"] + boardCatalog[i]["threads"][x]["omitted_images"])
      chanThread["lastModified"] = str(boardCatalog[i]["threads"][x]["last_modified"])
      chanThread["creationDate"] = str(boardCatalog[i]["threads"][x]["time"])
      threads.append(chanThread)
  return threads
