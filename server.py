"""
navichan - web application to display information of internet boards
Copyright (C) 2018 lain7

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import time
import json
from catalog import *
from config import *
from generators import *
from imageboards import *
from http.server import BaseHTTPRequestHandler
from os.path import isfile
from settings import WORKINGDIR
from sys import stderr
from traceback import print_exc as trace

class catalogServer(BaseHTTPRequestHandler):
  def send404(self):
      errorPage = open(WORKINGDIR + "web/simple/404.html", encoding="UTF-8")
      self.send_response(404)
      self.send_header("Content-type", "text/html")
      self.end_headers()
      self.wfile.write(bytes(errorPage.read(), "UTF-8"))
  def do_GET(self):
    # handle request #
    if (self.path == "/"):
      homePage = bytes(generateHome(), "UTF-8")
      self.send_response(200)
      self.send_header("Content-type", "text/html")
      self.end_headers()
      self.wfile.write(homePage)
    # Allow access to all images in the images folder #
    elif any(x in self.path[-5::] for x in [".png", ".jpg", ".jpeg", ".gif"]):
      if (isfile(WORKINGDIR + "web/images" + self.path)):
        imgFile = open(WORKINGDIR + "web/images" + self.path, "rb")
        imgBytes = imgFile.read()
        imgFile.close()
        for x in [".png", ".jpg", ".jpeg", ".gif"]:
          if (x in self.path[-5::]):
            self.send_response(200)
            self.send_header("Content-type", "image/" + x.split('.')[1])
            self.end_headers()
            self.wfile.write(imgBytes)
            break
      else:
        self.send404()
    elif (self.path == "/style.css"):
      cssFile = open(WORKINGDIR + "web/style.css", "rb")
      cssBytes = cssFile.read()
      cssFile.close()
      self.send_response(200)
      self.send_header("Content-type", "text/css")
      self.end_headers()
      self.wfile.write(cssBytes)
    elif (self.path[-5::] == ".html" ):
        if (isfile(WORKINGDIR + "web/simple" + self.path)):
          try:
            htmlPage = open(WORKINGDIR + "web/simple" + self.path, "r", encoding="UTF-8")
          except:
            stderr.write(time.asctime() + " - Could not open " + self.path + "!\n")
          self.send_response(200)
          self.send_header("Content-type", "text/html")
          self.end_headers()
          self.wfile.write(bytes(htmlPage.read(), "UTF-8"))
          htmlPage.close()
        else:
          self.send404()
    elif ("boards=" in self.path):
      try:
        self.catalog = catalog()
        self.path = self.path.replace('%2C', ',')
        self.path = self.path.replace('%3A', ':')
        self.path = self.path.replace("%23", '#')
        self.path = self.path.replace('%20', '')
        self.path = self.path.replace("+", '')
        # parse path #
        for arg in self.path.split('?')[1].split('&'):
          argSplit = arg.split('=')
          if (argSplit[0] == "boards"):
            self.catalog.boards = argSplit[1].split(',')
          elif (argSplit[0] == "sortMethod"):
            self.catalog.sortMethod = argSplit[1]
          elif (argSplit[0] == "filterFile"):
            self.catalog.filterFile = argSplit[1]
          elif (argSplit[0] == "theme"):
            self.catalog.theme = argSplit[1]
          elif (argSplit[0] == "bgColor"):
            self.catalog.bgColor = argSplit[1]
          elif (argSplit[0] == "fgColor"):
            self.catalog.fgColor = argSplit[1]
        # Correct non-specified boards to 4chan #
        for i in range(len(self.catalog.boards)):
          if (not "::" in self.catalog.boards[i]):
            self.catalog.boards[i] = "4chan::" + self.catalog.boards[i]
        # Get threads and remove unavailable boards #
        invalidBoards = 0
        boardsTemp = self.catalog.boards.copy()
        for board in self.catalog.boards:
          tempBoard = board.split("::")
          if (tempBoard[0] == "4chan"):
            self.catalog.threads += get4Catalog(tempBoard[1])
          elif (tempBoard[0] == "8chan"):
            self.catalog.threads += get8Catalog(tempBoard[1])
          elif (tempBoard[0] == "arisu"):
            self.catalog.threads += getArisuCatalog(tempBoard[1])
          else:
            stderr.write(time.asctime() + " - " + board + " is not an available board.\n")
            invalidBoards += 1
            # Send user to 404 if all requested boards are unavailable #
            if (invalidBoards == len(self.catalog.boards)):
              self.send404()
              return
            boardsTemp.remove(board)
        self.catalog.boards = boardsTemp
        # Filter Threads #
        if (not self.catalog.filterFile == "none"):
          self.catalog.threads = self.catalog.filter(self.catalog.threads, self.catalog.filterFile)
        # Sort Threads #
        if (len(self.catalog.boards) > 1 or self.catalog.sortMethod != "lastModified"):
          self.catalog.threads = self.catalog.sort(self.catalog.threads, self.catalog.sortMethod)
        # Set proper colors if theme #
        if (not self.catalog.theme == "none"):
          cfg = config()
          for i in range(len(cfg.themes)):
            if (cfg.themes[i]["name"] == self.catalog.theme):
              self.catalog.bgColor = cfg.themes[i]["bgColor"]
              self.catalog.fgColor = cfg.themes[i]["fgColor"]
              break
        # Serve webpage #
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(bytes(generateCatalog(self.catalog.threads, 
                                             str(self.catalog.boards).replace("/", '').replace("[", '').replace("]", '').replace("\'", ''), 
                                             self.catalog.sortMethod, 
                                             self.catalog.filterFile,
                                             self.catalog.theme,
                                             self.catalog.bgColor, 
                                             self.catalog.fgColor), "UTF-8"))
      except urllib.error.HTTPError:
        self.send404()
      except Exception as e:
        self.send_response(500)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        trace()
        self.wfile.write(bytes("<br>500 Error:<br> " + str(e) + "<br><br>Tell an admin.", "UTF-8"))
    else:
      self.send404()
