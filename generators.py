"""
navichan - web application to display information of internet boards
Copyright (C) 2018 lain7

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from config import *
from datetime import datetime
from settings import VERSION, WORKINGDIR

def generateHome():
  cfg = config()
  try:
    htmlRaw = open(WORKINGDIR + "web/home.html", "r", encoding="UTF-8")
  except FileNotFoundError:
    stderr.write(time.asctime() + " - Missing files or \"WORKINGDIR\" in settings.py is not configured!\n")
    return "Missing files or \"WORKINGDIR\" in settings.py is not configured!<br><br>Tell an admin."
  html = htmlRaw.read()
  favHtml = ""
  themeHtml = ""
  filterHtml = ""
  cfg.fixColors()  # During home page generation the '#' symbol causes the hex color to disappear #
                   # entirely from the path request. Replacing '#' with '%23' fixes this          #
  htmlRaw.close()
  count = 1
  favoriteLenMax = len(cfg.favorites)
  for i in range(len(cfg.favorites)):
    favHtml += "<a href=\"/?"
    if ("boards" in cfg.favorites[i]):
      favHtml += "boards=" + cfg.favorites[i]["boards"] + '&'
    if ("sortMethod" in cfg.favorites[i]):
      favHtml += "sortMethod=" + cfg.favorites[i]["sortMethod"] + '&'
    if ("theme" in cfg.favorites[i]):
      favHtml += "theme=" + cfg.favorites[i]["theme"] + '&'
    if ("bgColor" in cfg.favorites[i]):
     favHtml += "bgColor=" + cfg.favorites[i]["bgColor"] + '&'
    if ("fgColor" in cfg.favorites[i]):
      favHtml += "fgColor=" + cfg.favorites[i]["fgColor"] + '&'
    if ("filter" in cfg.favorites[i]):
      favHtml += "filterFile=" + cfg.favorites[i]["filter"] + '&'
    if ("name" in cfg.favorites[i]):
      favHtml += "\">" + cfg.favorites[i]["name"] + "</a>"
    if (count == 5):
      favHtml += "<br />"
      count = 1
    elif (not favoriteLenMax == i+1):
      favHtml += " | "
      count += 1
    else:
      count += 1
  for i in range(len(cfg.themes)):
    themeHtml += "<option value=\"" + cfg.themes[i]["name"] + "\">" + cfg.themes[i]["name"] + "</option>"
  for i in range(len(cfg.filters)):
    filterHtml += "<option value=\"" + cfg.filters[i] + "\">" + cfg.filters[i] + "</option>"
  html = html.replace("strFavorites", favHtml)
  html = html.replace("strThemes", themeHtml)
  html = html.replace("strFilters", filterHtml)
  html = html.replace("strVersion", VERSION)
  return html

def generateCatalog(threads, board, sortMethod, filterFile, theme, bgColor, fgColor):
  cfg = config()
  try:
    htmlRaw = open(WORKINGDIR + "web/catalog.html", "r", encoding="UTF-8")
  except FileNotFoundError:
    stderr.write(time.asctime() + " - Missing files or \"WORKINGDIR\" in settings.py is not configured!\n")
    return "Missing files or \"WORKINGDIR\" in settings.py is not configured!<br><br>Tell an admin."
  html = htmlRaw.read()
  threadsHtml = ""
  count = 0
  htmlRaw.close()
  # Generates each item in the catalog #
  for i in range(len(threads)):
    threadsHtml += "<div id=\"item\"><a href=\"" + threads[i]['link'] + "\"><div id=\"imgContainer\"><img src=\"" + threads[i]['img'] + "\" alt=\"404\"/></div></a>" + "<div class=\"text\">R: <strong>" + threads[i]["replies"] + "</strong> / I: <strong>" + threads[i]["images"] +"</strong></div><p>"
    if (threads[i]["title"] != ''):
      threadsHtml += "<strong>" + threads[i]["title"] + "</strong>"
      if(threads[i]["main"] != ''):
        threadsHtml += ": " + threads[i]["main"]
    else:
      threadsHtml += threads[i]["main"]
    threadsHtml += "</p></div>"
  html = html.replace("strBgColor", bgColor)
  html = html.replace("strFgColor", fgColor)
  for i in range(len(cfg.themes)):
    if (cfg.themes[i]["name"] == theme):
      try:
        html = html.replace("strInvertPercent", cfg.themes[i]["invertPercent"])
      except KeyError:
        html = html.replace("strInvertPercent", "0%")
      finally:
        break
  html = html.replace("strBoard", board)
  html = html.replace("strDayTime", str(datetime.now()))
  html = html.replace("strSortMethod", sortMethod)
  html = html.replace("strFilterFile", filterFile)
  html = html.replace("strThreads", threadsHtml)
  return html
