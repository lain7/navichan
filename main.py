#!/usr/bin/env python3
"""
navichan - web application to display information of internet boards
Copyright (C) 2018 lain7

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from config import *
from http.server import BaseHTTPRequestHandler, HTTPServer
from server import *
from settings import VERSION
from sys import argv, stderr
from textwrap import dedent

def main():
  cfg = config()
  # Handle Arguments #
  for i in range(len(argv)):
    if (argv[i] == "-v" or argv[i] == "--version"):
      print("Navichan Version " + VERSION)
      return 0
    elif (argv[i] == "-h" or argv[i] == "--help"):
      print(dedent("""\
          -v or --version                       Prints the verison number and exits
          -l or --license                       Shows the license and exits
          -h or --help                          Shows this help text and exits
          -c or --config [file path]            Sets the config manually
          -f or --filterdir [filter directory]  Sets the filter directory"""))
      return 0
    elif (argv[i] == "-l" or argv[i] == "--license"):
      print(dedent("""\
        navichan - web application to display information of internet boards              
        Copyright (C) 2018 lain7                                                          
                                                                                   
        This program is free software: you can redistribute it and/or modify              
        it under the terms of the GNU Affero General Public License as published by       
        the Free Software Foundation, either version 3 of the License, or                 
        (at your option) any later version.                                               
 
        This program is distributed in the hope that it will be useful,                   
        but WITHOUT ANY WARRANTY; without even the implied warranty of                    
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                      
        GNU Affero General Public License for more details.                               
 
        You should have received a copy of the GNU                                        
        along with this program. If not, see <http://www.gnu.org/licenses/>.              
        """))
      return 0
  # Create server #
  catServer = HTTPServer((cfg.defaults["host"], int(cfg.defaults["port"])), catalogServer)
  stderr.write(time.asctime() + " Server Starts - %s:%s" % (cfg.defaults["host"], int(cfg.defaults["port"])) + "\n")
  try:
    catServer.serve_forever()
  except KeyboardInterrupt:
    pass
  catServer.server_close()
  stderr.write(time.asctime() + " Server Stops - %s:%s" % (cfg.defaults["host"], int(cfg.defaults["port"])) + "\n")

if __name__ == "__main__":
  main()
