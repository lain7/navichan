"""
navichan - web application to display information of internet boards
Copyright (C) 2018 lain7

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import json
import time
from os.path import expanduser, isfile
from settings import WORKINGDIR
from sys import argv, stderr

class config():
  def __init__(self):
    # gets config #
    path = ""
    if (isfile(WORKINGDIR + "config.json")):
      path = "config.json"
    elif (isfile(expanduser("~/.config/navichan/config.json"))):
      path = ".config/navichan/config.json"
    for i in range(len(argv)):
      if (argv[i] == "-c" or argv[i] =="--config"):
        path = argv[i+1]
    try:
      configFile = open(path, 'r', encoding="UTF-8")
      config = json.loads(configFile.read())
      configFile.close()
    except FileNotFoundError:
      stderr.write(time.asctime() + " - config.json not found in current directory or ~/.config/navichan/\n")
      exit(1)
    except:
      stderr.write(time.asctime() + " - The file could not be read or was not a valid config file\n")
      exit(1)
    # sets variables #
    self.favorites = config["favorites"]
    self.defaults = config["defaults"]
    self.themes = config["themes"]
    self.filters = config["filters"]

  # Replaces # with %23 #
  def fixColors(self):
    for i in range(len(self.favorites)):
      if ("bgColor" in self.favorites[i] and "fgColor" in self.favorites[i]):
        self.favorites[i]["bgColor"] = self.favorites[i]["bgColor"].replace("#", '%23')
        self.favorites[i]["fgColor"] = self.favorites[i]["fgColor"].replace("#", '%23')
